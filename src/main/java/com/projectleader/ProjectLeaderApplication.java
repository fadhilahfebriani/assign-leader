package com.projectleader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectLeaderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectLeaderApplication.class, args);
		//System.out.println("Hello World");
	}

}

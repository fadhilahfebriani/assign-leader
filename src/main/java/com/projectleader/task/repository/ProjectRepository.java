package com.projectleader.task.repository;

import com.projectleader.task.domain.dao.ProjectDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectDao, Long> {
}

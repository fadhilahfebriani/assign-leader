package com.projectleader.task.repository;

import com.projectleader.task.domain.dao.EmployeeDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<EmployeeDao, Long> {
}

package com.projectleader.task.controller;

import com.projectleader.task.domain.dto.EmployeeDto;
import com.projectleader.task.domain.dto.ProjectDto;
import com.projectleader.task.service.AssignProjectService;
import com.projectleader.task.service.EmployeeService;
import com.projectleader.task.service.ProjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/project")
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @PostMapping("/create")
    public ResponseEntity<Object> create(@RequestBody ProjectDto req) {
        try {
            return new ResponseEntity<>(projectService.create(req), HttpStatus.CREATED);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update/{idProject}")
    public ResponseEntity update(@RequestBody ProjectDto req, @PathVariable Long idProject) {
        try {
            return new ResponseEntity<>(projectService.update(req, idProject), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{idEmployee}")
    public ResponseEntity findById(@PathVariable Long idEmployee) {
        try {
            return new ResponseEntity<>(projectService.findById(idEmployee), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ResponseBody
    @GetMapping("/get")
    public ResponseEntity<Object> getAll(ProjectDto req) {
        try {
            return new ResponseEntity<>(projectService.getAll(req), HttpStatus.CREATED);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity delete(@RequestBody ProjectDto req) {
        try {
            return new ResponseEntity<>(projectService.delete(req), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}

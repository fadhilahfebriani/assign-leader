package com.projectleader.task.controller;

import com.projectleader.task.domain.dto.AssignProjectDto;
import com.projectleader.task.domain.dto.ProjectDto;
import com.projectleader.task.service.AssignProjectService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/assign-project")
public class AssignProjectController {

    private final AssignProjectService assignProjectService;

    public AssignProjectController(AssignProjectService assignProjectService) {
        this.assignProjectService = assignProjectService;
    }

    @PostMapping("/assign-project")
    public ResponseEntity<Object> create(@RequestBody AssignProjectDto req) {
        try {
            return new ResponseEntity<>(assignProjectService.assignProject(req), HttpStatus.CREATED);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

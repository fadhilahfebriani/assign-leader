package com.projectleader.task.controller;

import com.projectleader.task.domain.dto.EmployeeDto;
import com.projectleader.task.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping("/create")
    public ResponseEntity<Object> create(@RequestBody EmployeeDto req) {
        try {
            return new ResponseEntity<>(employeeService.create(req), HttpStatus.CREATED);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/update/{idEmployee}")
    public ResponseEntity update(@RequestBody EmployeeDto req, @PathVariable Long idEmployee) {
        try {
            return new ResponseEntity<>(employeeService.update(req, idEmployee), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{idEmployee}")
    public ResponseEntity findById(@PathVariable Long idEmployee) {
        try {
            return new ResponseEntity<>(employeeService.findById(idEmployee), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ResponseBody
    @GetMapping("/get")
    public ResponseEntity<Object> getAll(EmployeeDto req) {
        try {
            return new ResponseEntity<>(employeeService.getAll(req), HttpStatus.CREATED);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete")
    public ResponseEntity delete(@RequestBody EmployeeDto req) {
        try {
            return new ResponseEntity<>(employeeService.delete(req), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

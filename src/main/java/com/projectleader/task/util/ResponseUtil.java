package com.projectleader.task.util;

import com.projectleader.task.config.MessageResourceConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;

public class ResponseUtil {

    private static final MessageSource messageSource = (new MessageResourceConfiguration()).messageSource();
    private ResponseUtil() {
    }

    public static <T extends Serializable> ResponseEntity<Object> buildResponse(String key, String message, Serializable data, HttpStatus httpStatus) {
        return new ResponseEntity(buildResponse(key, message, data, httpStatus).getStatusCode());
    }


}

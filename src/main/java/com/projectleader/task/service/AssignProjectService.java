package com.projectleader.task.service;

import com.projectleader.task.constant.Constant;
import com.projectleader.task.domain.dao.EmployeeDao;
import com.projectleader.task.domain.dao.ProjectDao;
import com.projectleader.task.domain.dto.AssignProjectDto;
import com.projectleader.task.domain.dto.ProjectDto;
import com.projectleader.task.domain.dto.Response;
import com.projectleader.task.repository.ProjectRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class AssignProjectService {

    private final ProjectRepository projectRepository;

    public Response assignProject(AssignProjectDto req) throws Exception {
        Response response = new Response();
        ProjectDao exist = projectRepository.findById(req.getIdProject()).orElse(null);

        if (!ObjectUtils.isEmpty(exist)) {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage("User Doesn't Exist");
            response.setData(null);

            return response;
        }

        log.info(">>> Start Process Create Project");

        Set<EmployeeDao> members = new HashSet<>();
        req.getMembers().forEach(x -> {
            members.add(EmployeeDao.builder()
                            .idEmployee(x.getIdEmployee())
                            .name(x.getName())
                            .position(x.getPosition())
                            .address(x.getAddress())
                    .build());
        });

        // Set data to be save
        ProjectDao data = ProjectDao.builder()
                .idProject(req.getIdProject())
                .projectName(req.getProjectName())
                .projectLeader(req.getProjectLeader())
                .members(members)
                .build();


        // Save data to Database
        ProjectDao dao = projectRepository.save(data);

        ProjectDto res = new ProjectDto(data.getIdProject(), data.getProjectName(), data.getProjectLeader());
        response.setResponseCode(HttpStatus.CREATED.value());
        response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
        response.setData(dao);

        return response;

    }


}

package com.projectleader.task.service;

import com.projectleader.task.domain.dto.EmployeeDto;

public interface EmployeeService {

    @SuppressWarnings("all")
    Object create(EmployeeDto req) throws Exception;

    Object update(EmployeeDto req, Long idEmployee) ;

    Object findById(Long idEmployee) ;

    Object getAll(EmployeeDto req);

    Object delete(EmployeeDto req);
}

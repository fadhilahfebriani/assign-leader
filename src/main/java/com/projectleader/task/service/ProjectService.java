package com.projectleader.task.service;

import com.projectleader.task.domain.dto.EmployeeDto;
import com.projectleader.task.domain.dto.ProjectDto;

public interface ProjectService {

    @SuppressWarnings("all")
    Object create(ProjectDto req) throws Exception;

    Object update(ProjectDto req, Long idProject) ;

    Object findById(Long idProject) ;

    Object getAll(ProjectDto req);

    Object delete(ProjectDto req);
}

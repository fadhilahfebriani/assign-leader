package com.projectleader.task.service.impl;

import com.projectleader.task.constant.Constant;
import com.projectleader.task.domain.dao.EmployeeDao;
import com.projectleader.task.domain.dao.ProjectDao;
import com.projectleader.task.domain.dto.EmployeeDto;
import com.projectleader.task.domain.dto.ProjectDto;
import com.projectleader.task.domain.dto.Response;
import com.projectleader.task.repository.EmployeeRepository;
import com.projectleader.task.repository.ProjectRepository;
import com.projectleader.task.service.ProjectService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@SuppressWarnings("all")
public class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Response create(ProjectDto req) throws Exception {
        Response response = new Response();
        ProjectDao exist = projectRepository.findById(req.getIdProject()).orElse(null);

        if (!ObjectUtils.isEmpty(exist)) {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage("User Doesn't Exist");
            response.setData(null);

            return response;
        }

        // Set data to be save
        ProjectDao data = ProjectDao.builder()
                .idProject(req.getIdProject())
                .projectName(req.getProjectName())
                .projectLeader(req.getProjectLeader())
                .build();


        // Save data to Database
        ProjectDao dao = projectRepository.save(data);

        ProjectDto res = new ProjectDto(data.getIdProject(), data.getProjectName(), data.getProjectLeader());
        response.setResponseCode(HttpStatus.CREATED.value());
        response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
        response.setData(dao);

        return response;

    }

    @Override
    public Response update(ProjectDto req, Long idProject) {
        Response response = new Response();
        ProjectDao exist = projectRepository.findById(idProject).orElse(null);

        if(ObjectUtils.isEmpty(exist)) {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage("User Doesn't Exist");
            response.setData(null);
            return response;
        }

        // Set data to be save
        ProjectDao data = ProjectDao.builder()
                .idProject(req.getIdProject())
                .projectName(req.getProjectName())
                .projectLeader(req.getProjectLeader())
                .build();


        // Save data to Database
        ProjectDao dao = projectRepository.save(data);

        ProjectDto res = new ProjectDto(data.getIdProject(), data.getProjectName(), data.getProjectLeader());
        response.setResponseCode(HttpStatus.CREATED.value());
        response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
        response.setData(dao);

        return response;
    }

    public Response findById(Long idProject) {
        Response response = new Response();
        Optional<ProjectDao> data = projectRepository.findById(idProject);
        if(data.isPresent()) {
            response.setResponseCode(HttpStatus.OK.value());
            response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
            response.setData(data);
        } else {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage(Constant.ResponseKey.KEY_DATA_NOT_FOUND);
            response.setData(null);
        }

        return response;
    }

    public Response getAll(ProjectDto req) {
        Response response = new Response();
        List<ProjectDao> data = projectRepository.findAll();
        response.setResponseCode(HttpStatus.OK.value());
        response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
        response.setData(data);

        return response;
    }

    public Response delete(ProjectDto req) {
        Response response = new Response();
        log.info(">>> Start Process Soft Delete Employee {}", req.getIdProject());

        if(req.getIdProject() == null) {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage(Constant.ResponseKey.KEY_INVALID_REQUEST);
            response.setData(null);
        }

        Optional<ProjectDao> exist = projectRepository.findById(req.getIdProject());
        if(exist.isEmpty()) {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage(Constant.ResponseKey.KEY_DATA_NOT_FOUND);
            response.setData(null);
        }

        ProjectDao data = exist.get();
        data.setIsDeleted(Constant.IsDeleted.IS_DELETED_YES);
        projectRepository.saveAndFlush(data);

        response.setResponseCode(HttpStatus.OK.value());
        response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
        response.setData(data);

        return response;
    }

}


package com.projectleader.task.service.impl;

import com.projectleader.task.constant.Constant;
import com.projectleader.task.domain.dao.EmployeeDao;
import com.projectleader.task.domain.dto.EmployeeDto;
import com.projectleader.task.domain.dto.Response;
import com.projectleader.task.repository.EmployeeRepository;
import com.projectleader.task.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;


@Slf4j
@Service
@SuppressWarnings("all")
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Response create(EmployeeDto req) throws Exception {
        Response response = new Response();
        EmployeeDao exist = employeeRepository.findById(req.getIdEmployee()).orElse(null);

        if (!ObjectUtils.isEmpty(exist)) {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage("User Doesn't Exist");
            response.setData(null);

            return response;
        }

        // Set data to be save
        EmployeeDao data = EmployeeDao.builder()
                .idEmployee(req.getIdEmployee())
                .name(req.getName())
                .position(req.getPosition())
                .address(req.getAddress())
                .build();


        // Save data to Database
        EmployeeDao dao = employeeRepository.save(data);

        EmployeeDto res = new EmployeeDto(data.getIdEmployee(), data.getName(), data.getPosition(), data.getAddress());
        response.setResponseCode(HttpStatus.CREATED.value());
        response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
        response.setData(dao);

        return response;

    }

    public Response getAll(EmployeeDto req) {
        Response response = new Response();
        List<EmployeeDao> data = employeeRepository.findAll();
        response.setResponseCode(HttpStatus.OK.value());
        response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
        response.setData(data);

        return response;
    }

    public Response findById(Long idEmployee) {
        Response response = new Response();
        Optional<EmployeeDao> data = employeeRepository.findById(idEmployee);
        if(data.isPresent()) {
            response.setResponseCode(HttpStatus.OK.value());
            response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
            response.setData(data);
        } else {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage(Constant.ResponseKey.KEY_DATA_NOT_FOUND);
            response.setData(null);
        }

        return response;
    }

    @Override
    public Response update(EmployeeDto req, Long idEmployee) {
        Response response = new Response();
        EmployeeDao exist = employeeRepository.findById(idEmployee).orElse(null);

        if(ObjectUtils.isEmpty(exist)) {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage("User Doesn't Exist");
            response.setData(null);
            return response;
        }

        // Set data to be save
        EmployeeDao data = EmployeeDao.builder()
                .idEmployee(req.getIdEmployee())
                .name(req.getName())
                .position(req.getPosition())
                .address(req.getAddress())
                .build();


        // Save data to Database
        EmployeeDao dao = employeeRepository.save(data);

        EmployeeDto res = new EmployeeDto(data.getIdEmployee(), data.getName(), data.getPosition(), data.getAddress());
        response.setResponseCode(HttpStatus.CREATED.value());
        response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
        response.setData(dao);

        return response;
    }

    public Response delete(EmployeeDto req) {
        Response response = new Response();
        log.info(">>> Start Process Soft Delete Employee {}", req.getIdEmployee());

        if(req.getIdEmployee() == null) {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage(Constant.ResponseKey.KEY_INVALID_REQUEST);
            response.setData(null);
        }

        Optional<EmployeeDao> exist = employeeRepository.findById(req.getIdEmployee());
        if(exist.isEmpty()) {
            response.setResponseCode(HttpStatus.BAD_REQUEST.value());
            response.setMessage(Constant.ResponseKey.KEY_DATA_NOT_FOUND);
            response.setData(null);
        }

        EmployeeDao data = exist.get();
        data.setIsDeleted(Constant.IsDeleted.IS_DELETED_YES);
        employeeRepository.saveAndFlush(data);

        response.setResponseCode(HttpStatus.OK.value());
        response.setMessage(Constant.ResponseKey.KEY_SUCCESS);
        response.setData(data);

        return response;
    }

}

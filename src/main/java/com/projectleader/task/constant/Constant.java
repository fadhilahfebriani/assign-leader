package com.projectleader.task.constant;

public class Constant {
    public class ResponseKey {
        private ResponseKey() {
        }

        public static final String KEY_SUCCESS = "SUCCESS";

        public static final String KEY_SUKSES = "SUKSES";
        public static final String KEY_FAILED = "FAILED";
        public static final String KEY_GAGAL = "GAGAL";
        public static final String KEY_DATA_NOT_FOUND = "DATA_NOT_FOUND";
        public static final String KEY_DATA_CUSTOMER_IN_REDIS_IS_NULL = "KEY_DATA_CUSTOMER_IN_REDIS_IS_NULL";
        public static final String GENERAL_ERROR = "GENERAL_ERROR";
        public static final String KEY_INVALID_DATA_FORMAT = "INVALID_DATA_FORMAT";
        public static final String KEY_INVALID_REQUEST = "INVALID_REQUEST";
        public static final String KEY_ALREADY_EXIST = "DATA_ALREADY_EXIST";

    }

    public class ResponseMessageKey {
        private ResponseMessageKey() {
        }

        public static final String MESSAGE_KEY_SUCCESS = "api.success";
        public static final String MESSAGE_KEY_FAILED = "api.failed";
        public static final String MESSAGE_KEY_NOTFOUND = "api.notfound";
        public static final String MESSAGE_KEY_UNKNOWN_ERROR = "default.internal.server.error";
    }

    public static class IsDeleted {
        private IsDeleted() {
        }

        public static final String IS_DELETED_NO = "N";
        public static final String IS_DELETED_YES = "Y";
        public static final Integer NO_IS_DELETED = 0;
        public static final Integer YES_IS_DELETED = 1;
    }
}

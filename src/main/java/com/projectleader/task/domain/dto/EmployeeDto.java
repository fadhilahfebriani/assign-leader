package com.projectleader.task.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.projectleader.task.domain.dao.EmployeeDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeDto implements Serializable {

    private static final long serialVersionUID = 8192861012375876746L;

    private Long idEmployee;

    private String name;

    private String position;

    private String address;

//    public EmployeeDto(EmployeeDao entity) {
//        this.idEmployee = entity.getIdEmployee();
//        this.name = entity.getName();
//        this.position = entity.getPosition();
//        this.address = entity.getAddress();
//    }

}

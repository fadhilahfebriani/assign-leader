package com.projectleader.task.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.projectleader.task.domain.dao.ProjectDao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectDto implements Serializable {
    private static final long serialVersionUID = 5620363275951963640L;

    private Long idProject;

    private String projectName;

    private String projectLeader;

//    private List<EmployeeDto> members = new ArrayList<>();

//    public ProjectDto(ProjectDao entity) {
//        this.idProject = entity.getIdProject();
//        this.projectName = entity.getProjectName();
//        this.projectLeader = entity.getProjectLeader();
//        this.members = entity.getMembers().stream().map(EmployeeDto::new).collect(Collectors.toList());
//    }
}

package com.projectleader.task.domain.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name ="EMPLOYEE")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeDao implements Serializable {
    private static final long serialVersionUID = 4457854527385569561L;

    @Id
    @Column(name = "ID_EMPLOYEE")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long idEmployee;

    @Column(name = "NAME")
    private String name;

    @Column(name = "POSITION")
    private String position;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "IS_DELETED")
    private String isDeleted;
}

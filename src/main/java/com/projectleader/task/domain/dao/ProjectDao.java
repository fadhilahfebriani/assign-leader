package com.projectleader.task.domain.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@Entity(name ="PROJECT")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectDao implements Serializable {
    private static final long serialVersionUID = -7263464202831570463L;

    @Id
    @Column(name = "ID_PROJECT")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long idProject;

    @Column(name = "PROJECT_NAME")
    private String projectName;

    @Column(name = "PROJECT_LEADER")
    private String projectLeader;

    @Column(name = "IS_DELETED")
    private String isDeleted;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "ID_PROJECT", referencedColumnName = "ID_PROJECT")
    private Set<EmployeeDao> members = new HashSet<>();


}

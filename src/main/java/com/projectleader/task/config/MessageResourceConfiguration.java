package com.projectleader.task.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Configuration("messageResourceConfigurationCommon")
public class MessageResourceConfiguration {
    @Bean("messageSourceCommon")
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource
                = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(
                "classpath:/messages/common"
        );
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }
}

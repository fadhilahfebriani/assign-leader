package com.projectleader.task.config;

public enum GenericResponseKey {

    SUCCESS,
    INTERNAL_SERVER_ERROR,
    GATEWAY_TIMEOUT_ERROR,
    UNKNOWN_ERROR,
    GENERAL_ERROR,

    FAILED,

    KEY_DATA_NOT_FOUND,

    KEY_INVALID_REQUEST,
    IN_PROGRESS;


    public String getResponseKey() {
        return this.toString();
    }
}

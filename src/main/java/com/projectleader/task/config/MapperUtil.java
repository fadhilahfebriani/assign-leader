package com.projectleader.task.config;

import lombok.extern.slf4j.Slf4j;
import java.util.ArrayList;
import java.util.stream.Collectors;

@Slf4j
public class MapperUtil {

    private MapperUtil() {
    }

//    public static Object transform(Object source, Object dest, boolean ambiguityIgnored) {
//        ModelMapper modelMapper = new ModelMapper();
//        modelMapper.getConfiguration().setAmbiguityIgnored(ambiguityIgnored);
//        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
//        try {
//            if (source instanceof ArrayList) {
//                return ((ArrayList) source).stream()
//                        .map(data -> modelMapper.map(data, dest.getClass()))
//                        .collect(Collectors.toList());
//            }
//            modelMapper.map(source, dest);
//        } catch (Exception ex) {
//            log.error("Error when transform data using model mapper. Error : ", ex);
//        }
//        return dest;
//    }
}
